<?php

namespace XDClient;

require_once('Emitter.php');

interface WebSocketHandler {
    public function pushMsg($data);
}

class WebSockEmitter implements Emitter {
    private $handler;

    public function __construct(WebSocketHandler $handler) {
        $this->handler = $handler;
    }

    public function emit($data) {
        $this->handler->pushMsg(json_encode($data));
    }

    public function emitError($msg) {
        $this->handler->pushMsg(json_encode(["error"=>$msg]));
    }
}

?>    
