<?php 

namespace XDClient;

require_once('Base.php');
require_once('Emitter.php');

use React\Socket\ConnectionInterface;

// 271117 adding idekey and making vars multidimensional
// (indexed by idekey)
class VarWatcher extends Base {

    protected $vars;

    public function __construct(Emitter $emitter) {
        parent::__construct();
        $this->emitter = $emitter;
        $this->vars = [];
    }

    public function processCommand(ConnectionInterface $conn, $cmd, $doc) {
        if(parent::processCommand($conn, $cmd, $doc)===true) {
            return true;
        }

        $handled = true;
        switch($cmd) {

            case "context_get": 
                if(!$doc->error) {
                    for($i=0; $i<count($doc->property); $i++) {
                        $this->processVar($doc, $i);
                    }
                    $this->emitter->emit( [ 
                                            "cmd"=>"line", "data"=>
                                        ["lineno"=>$this->lineno,
                                        "vars"=>
                                            isset($this->vars[$this->idekey]) ?
                                            $this->vars[$this->idekey]: [] ]]);
                } else {
                    $this->emitter->emitError($doc->error->message);
                }
                $this->onLineNo($conn, $this->lineno);
                $this->nextLine($conn);
                break;

            case "feature_set":
                $this->nextLine($conn);
                break;

            case "stdout":
                break;

            default:
                $this->emitter->emitError("Unrecognised command $cmd");
                $handled = false;
        }
        return $handled;
    }

    public function processVar($doc, $i) {
        $result = true;
        $n = (string)$doc->property[$i]["name"];
        switch($doc->property[$i]["type"]) {
            case "string":
                $this->handleString($n, $doc->property[$i]);
                break;

            case "int":
            case "float":
                $this->handleNumber($n, $doc->property[$i]);
                break;

            case "bool":
                $this->handleBoolean($n, $doc->property[$i]);
                break;

            case "array":
            case "hash":
                $this->handleArray($n, $doc->property[$i]);
                break;
           
            case "object":
                $this->handleObject($n, $doc->property[$i]);
                break;

            case "uninitialized":
                $this->handleUninitialized($n, $doc->property[$i]);
                break;

            default:
                $result = false;
        }
        return $result;
    }

   

    public function handleString($n, $prop) {
        $this->vars[$this->idekey]["$n"] = ["type"=>"string",
                        "value"=>base64_decode($prop)];
    }

    public function handleNumber($n, $prop) {
        $this->vars[$this->idekey]["$n"] = ["type"=>(string)$prop["type"],
                        "value"=>(string)$prop];
    }

    public function handleBoolean($n, $prop) {
        $this->vars[$this->idekey]["$n"] = ["type"=>"bool",
                        "value"=>$prop==1?"true":"false"];
    }

        // BUG 
        // not everything is a base64 encoded string!!!
    public function handleArray($n, $prop) {
        $this->vars[$this->idekey][$n] = ["type"=>(string)$prop["type"],
                                    "value"=>[]];
        for($j=0; $j<count($prop->property); $j++) {
            $this->vars[$this->idekey][$n]["value"]
                [(string)$prop->property[$j]["name"]] = 
             (isset($prop->property[$j]["encoding"]) &&
                (string)$prop->property[$j]["encoding"]=="base64") ?
                      base64_decode($prop->property[$j]):
                        (string)$prop->property[$j];
        }
    }

    public function handleObject($n, $prop) {
        $this->vars[$this->idekey][$n] = ["type"=>"object","classname"=>(string)$prop["classname"], "value"=>[]];
        for($j=0; $j<count($prop->property); $j++) {
            $this->vars[$this->idekey][$n]["value"]
                [(string)$prop->property[$j]["name"]] = 
             (isset($prop->property[$j]["encoding"]) &&
                (string)$prop->property[$j]["encoding"]=="base64") ?
                      base64_decode($prop->property[$j]):
                        (string)$prop->property[$j];
        }
    }

    public function onLineNo(ConnectionInterface $conn, $lineno) {
    }

    public function handleUninitialized($n, $prop) {
        /* no longer necessary as indexing vars by idekey
        if(isset($this->vars[$n])) {
            unset ($this->vars[$n]);
        }
        */
    }
    
    public function onStop($idekey) {
        $this->emitter->emit(['cmd'=>'stop','idekey'=>$idekey]);
        $this->vars[$idekey] = [];
    }

    public function processStream($streamContentsDecoded) {
        $this->emitter->emit(["cmd"=>"stdout","data"=>$streamContentsDecoded]);
    }

    public function handleIdeKeyOnSockChange($idekey) {    
        $this->emitter->setUser($idekey);
    }
}    

?>

        
