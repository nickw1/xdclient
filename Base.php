<?php 

namespace XDClient;

use React\Socket\ConnectionInterface;

abstract class Base {

    protected $copyStdout, $log, $idekey;
    protected $nextConnId, $idCounter;

    public function __construct($cs = true) {
        $this->copyStdout = $cs;
        $this->idCounter = 0;
        $this->idekey = null;
    }
 
    public function processData(ConnectionInterface $conn, $data) {

            $this->idekey = isset($conn->idekey) ? $conn->idekey: null;
            if($this->idekey !== null) {
                $this->handleIdeKeyOnSockChange($this->idekey);
            }

            // Get the idekey associated with this connection. If there 
            // is one, do something with it with handleIdeKeyOnSockChange()
            // (this might set the username of an emitter to this idekey, for
            // instance, so that the XML processing emits the correct user) 
            /* TODO
            $idekey = $conn->getIdeKey();
            if($idekey!==null) {
                $this->handleIdeKeyOnSockChange($idekey);
            }
            */
            $ret = $this->processXML($conn, $data);
            if($this->idekey === null && is_string($ret)) { // init packet, got idekey
                // .. associate socket with idekey 
                $this->idekey = $ret;
                $this->handleIdeKeyOnSockChange($ret);
                if(!isset($conn->idekey)) {
                    $conn->idekey = $ret;
                }
                return true;
            }  
            return $ret; 
    }

    // processXML() returns an IDE key if one is found, or false in all
    // other cases. We can use this to set the IDE key when we encounter an
    // <init> tag.
    public function processXML(ConnectionInterface $conn, $xml) {
        $doc = simplexml_load_string($xml);
        echo "Got some XML: tag={$doc->getName()}\n";
        if($doc->getName()=="init") {
            // If onInit() returns false something went wrong so we should
            // IN NO CIRCUMSTANCES continue with the debug session
            if(($idekey=$this->onInit($doc))!==false) {
                echo "IDE key=$idekey\n";
                if($this->copyStdout) {
                    $this->setCopyStdout($conn);
                } else {
                    $this->nextLine($conn);
                }
                return $idekey;
            } else {
                return false;
            } 
        } elseif($doc->getName()=="response") {
            $cmd = (string)$doc["command"];
            if($cmd=="stdout" && (string)$doc["success"]=="1") {
                echo "Received a stdout command\n";
                $this->nextLine($conn);
            }
            $this->processCommand($conn, $cmd, $doc);
        } elseif ($doc->getName()=="stream") {
            echo "Received a stream command\n";
            $this->processStream(base64_decode((string)$doc));
        }
        return true;
    }

    public function processCommand(ConnectionInterface $conn, $cmd, $doc) {
        $handled = false;
        echo "processCommand(): command=$cmd\n";
        switch($cmd) {
            case 'step_into':
                if($doc['status']=='stopping') {
                    $this->removeConn($conn);
                    $conn->close();
                }
                $msg = $doc->children('xdebug',true);
                if($msg->message) {
                    $this->lineno = (int)$msg->attributes()->lineno;
                    $this->getContextInfo($conn);
                }
                $handled=true;
                break;
        }
        return $handled;
    }

    // In VarWatcher
    abstract public function processStream($streamContentsDecoded);

    public function setCopyStdout(ConnectionInterface $conn) {
        $cmd = "stdout -i ". $this->nextId()." -c 1";
        echo "Sending a stdout command back: $cmd\n";
        $conn->write($cmd."\0");
    }

    public function nextLine(ConnectionInterface $conn) {
        $cmd = "step_into -i ". $this->nextId();
        echo "Sending a nextLine command back $cmd\n";
        $conn->write($cmd."\0");
    }

    public function onInit($doc) {
        return (string)$doc["idekey"];
    }

    // in VarWatcher
    abstract public function onStop($idekey);


    // in VarWatcher
    abstract function handleIdeKeyOnSockChange($idekey);


    // To allow manual removeConn, e.g. encountering an SQL error, or
    // an init error e.g. attempt to debug a non-user script
    protected function removeConn(ConnectionInterface $conn) {
        // If we get to the end of the debug session we need
        // to both removeConn the stream socket and remove it
        // from the array of sockets
        $idekey= isset($conn->idekey) ? $conn->idekey: null;
        // TODO close the conn
        if($idekey !== null) {
            // idekey will be false if not assigned yet, e.g. shutting down
            // due to unacceptable init packet such as trying to debug a
            // non-user script
            $this->onStop($idekey);
        } 
        
    }
    
    public function getContextInfo(ConnectionInterface $conn) {
        $cmd = "context_get -i ".$this->nextid();
        echo "Sending a context_get command back: $cmd\n";
        $conn->write($cmd."\0");
    }

    protected function nextId() {
        return ++$this->idCounter;
    }
}

?>
