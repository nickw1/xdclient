<?php
namespace XDClient;

require_once('WebSockEmitter.php');

class MultiUserEmitter extends WebSockEmitter {
    protected $user;

    public function __construct(WebSocketHandler $handler) {
        parent::__construct($handler);
        $this->user = null;
    }

    public function emit($data) {
        $data1 = $this->addUserToData($data);
        parent::emit($data1);
    }

    public function setUser($user) {
        $this->user=$user;
    }

    protected function addUserToData($data) {
        $m= array_merge(["user"=>$this->user],$data);
        return $m;
    }
}

?>
