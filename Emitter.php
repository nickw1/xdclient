<?php

namespace XDClient;

interface Emitter {
    public function emit($data);
    public function emitError($msg);
}

class BasicEmitter implements Emitter {
    public function emit($data) {
        echo json_encode($data)."\n";
    }

    public function emitError($msg) {
        echo json_encode(["error"=>$msg])."\n";
    }

}


?>
